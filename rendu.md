# Introduction à la sécurité informatique
## [Rendu] Travaux pratiques : les droits d’accès dans les systèmes UNIX

#### Binome :

- Nom, Prénom, email : El Idrissi, Elyas, elyas.elidrissi.etu@univ-lille.fr
- Nom, Prénom, email : Guffroy, Cyril, cyril.guffroy.etu@univ-lille.fr

#### Question 1 :

Si l'`EUID` du processus n'a pas été modifié, alors le processus ne peut pas écrire.

En effet, `EUID` corresponderait à celui propriaitaire du fichier à savoir `toto`. Or le premier triplet des permissions ne donne pas les droits d'écritures au propriétaire du fichier.

#### Question 2 :

##### Question 2.1 :

Le charatère `x` pour un dossier permet à l'utilisateur concerné d'entrer dans le répertoire et d'accéder aux fichiers et aux répertoires qu'il contient.

##### Question 2.2 :

Il n'est possible d'entrer dans le répertoire `mydir` depuis l'utilisateur `toto`. En effet, l'`EUID` du processus `cd` est `toto` or le propriaitaire du répertoire `mydir` est `ubuntu`. Neamoins l'`EGID` du processus `cd` est `ubuntu` et le répertoire `mydir` à pour groupe également `ubuntu`. C'est donc le deuxième triplet des permissions qui va être utilisé. Celui ci ne permet pas les droits d'éxecution au groupe `ubuntu`.

##### Question 2.3 :

Le deuxième triplet des permissions ne donne pas les droits d'exécutions au groupe `ubuntu` mais donne les droits de lecture. De ce faite on obtient une vue partiel de la commande `ls` car selon la documentation, `ls` utilise la commande `stat` qui a besoin des droits d'exécutions pour obtenir les informations des fichiers/dossiers.

```
ls: cannot access 'mydir/.': Permission denied
ls: cannot access 'mydir/..': Permission denied
ls: cannot access 'mydir/data.txt': Permission denied
total 0
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? data.txt
```

#### Question 3 :

##### Question 3.1 :

Les valeurs des ids sont toutes de `1001` ce qui correspond au groupe `toto`. Le programme n'arrive pas à ouvrir le fichier `mydir/data.txt` car le progamme a été lancé avec les droits du groupe de l'utilisateur à l'origine de son exécution (c-à-d `toto`). Le groupe `toto` n'ayant pas les droits d'execution sur le dossier `mydir/`, le fichier `mydir/data.txt` n'a pas pu être lu par le programme.

```
EUID = 1001
EGID = 1001
RUID = 1001
RGID = 1001
Cannot open file: Permission denied
```

##### Question 3.2 :

Les valeurs sont de `1001` (`toto`) pour `EGID`, `RUID`, `RGID` et de `1000`pour `EUID`.
Le programme a réussi à lire le contenu du fichier `mydir/data.txt` car avec le flag `set-group-id` le programme a été lancé avec `EUID` égal à l'id du propritaire (à savoir `ubuntu`) qui dispose des droits d'exécution sur le dossier `mydir` ; lui permettant donc de lire le fichier `mydir/data.txt`.

```
EUID = 1000
EGID = 1001
RUID = 1001
RGID = 1001
File opens correctly :
Hello World
```

#### Question 4 :

##### Question 4.1 :

Lorsque l'utilisateur est *Ubuntu*, les ids sont de 1000, que ce soit pour l'EUID ou l'EGID. 
En lançant le script python avec l'utilisateur *toto* (appartenant au groupe *ubuntu*), on a un EUID de 1001, et un EGID de 1000.

##### Question 4.2 :

Un utilisateur peut changer un de ses attributs sans demander à l’administrateur avec la commande `chfn`.

#### Question 5 :

##### Question 5.1 :

La commande `chfn` permet à un utilisateur de changer un de ses attributs du fichier `/etc/passwd` sans être root.

##### Question 5.2 :

Le résultat de `ls -al /usr/bin/chfn` est `-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn`. L'utilisateur root à les droits de lecture, d'écriture et d'exécution. Le groupe root à les droits de lecture et d'exécution. Les autres ont également les droits de lecture et d'exécution.
Neamoins le premier triplet des permissions possède le flag `set-group-id` ce qui fait que le programme se lance avec l'`EUID` égal à l'id du propritaire, c'est à dire `root`. Cela permet aux autres utilisateurs de pouvoir exécuter le programme.

Avant l'utilisation de `chfn` `/etc/passwd` était le suivant :

```
root:x:0:0:root:/root:/bin/bash
[...]
ubuntu:x:1000:1000:Ubuntu:/home/ubuntu:/bin/bash
lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
toto:x:1001:1001::/home/toto:/bin/bash
```

Après l'utilisation de `chfn` `/etc/passwd` est le suivant :

```
root:x:0:0:root:/root:/bin/bash
[...]
ubuntu:x:1000:1000:Ubuntu:/home/ubuntu:/bin/bash
lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
toto:x:1001:1001:,1,0102030405,0504030201:/home/toto:/bin/bash
```

Les informations ont donc bien été mises à jour.

#### Question 6 :

Les mots de passe des utilisateurs sont stockés dans le fichier `/etc/shadow` pour augementer la sécurité car ce fichier n'est accessible que par l'utilisateur `root`.

#### Question 7 :

Liste des commandes :

__Création des groupes__
- groupadd groupe_a
- groupadd groupe_b
- groupadd admin_serveurPartage

__Création des répertoires__
- mkdir dir_a
- mkdir dir_b
- mkdir dir_c

__Création des utilisateurs__
- useradd -G groupe_a lambda_a
- useradd -G groupe_b lambda_b
- useradd -G admin_serveurPartage adminServer

__L'admin devient owner des dossiers__
- chown adminServer dir_a
- chown adminServer dir_b
- chown adminServer dir_c

__Changement des groupes des dossiers__
- chgrp groupe_a dir_a
- chgrp groupe_b dir_b
- chgrp admin_serveurPartage dir_c

__Changement des permissions__
- chmod 770 dir_a
- chmod +t dir_a
- chmod 770 dir_b
- chmod +t dir_b
- chmod 774 dir_c
- chmod +t dir_c
