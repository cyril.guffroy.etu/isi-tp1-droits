#!/bin/bash

# Création des groupes
groupadd groupe_a
groupadd groupe_b
groupadd admin_serveurPartage

# Création des répertoires
mkdir dir_a
mkdir dir_b
mkdir dir_c

# Création des utilisateurs
useradd -G groupe_a lambda_a
useradd -G groupe_b lambda_b
useradd -G admin_serveurPartage adminServer

# L'admin devient owner des dossiers
chown adminServer dir_a
chown adminServer dir_b
chown adminServer dir_c

# Changement des groupes des dossiers
chgrp groupe_a dir_a
chgrp groupe_b dir_b
chgrp admin_serveurPartage dir_c

# Changement des permissions
chmod 770 dir_a
chmod +t dir_a
chmod 770 dir_b
chmod +t dir_b
chmod 774 dir_c
chmod +t dir_c