#!/usr/bin/env python
#-*- coding: utf-8 -*-
import os

def main():
    euid = os.geteuid()
    egid = os.getegid()
    print("EUID : ", euid)
    print("EGID : ", egid)

if __name__ == '__main__':
    main()