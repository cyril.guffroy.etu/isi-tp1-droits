#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

char file_relative_path [] = "mydir/data.txt";

int main(int argc, char *argv[])
{
    printf("EUID = %d\n", geteuid());
    printf("EGID = %d\n", getegid());
    printf("RUID = %d\n", getuid());
    printf("RGID = %d\n", getgid());

    FILE * file;
    file = fopen(file_relative_path, "r");

    if (file == NULL)
    {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }

    printf("File opens correctly :\n");

    char c = fgetc(file);
    while (c != EOF)
    {
        printf ("%c", c);
        c = fgetc(file);
    }

    fclose(file);
    exit(EXIT_SUCCESS);
}